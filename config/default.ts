export default {
    port:1300,
    host:'localhost',
    dbUri:'',
    database:'RL-Test',
    userName:'dbConnect',
    password:'dbConnect',
    options:{
        dialect:'mssql',
    },
    cors:{
        origin: 'http://localhost:4200',
        optionsSuccessStatus: 200 // For legacy browser support
    }
}