USE [master]
GO
/****** Object:  Database [RL-Test]    Script Date: 1/08/2022 7:39:42 pm ******/
CREATE DATABASE [RL-Test]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RL-Test', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\RL-Test.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RL-Test_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\RL-Test_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RL-Test] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RL-Test].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RL-Test] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RL-Test] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RL-Test] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RL-Test] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RL-Test] SET ARITHABORT OFF 
GO
ALTER DATABASE [RL-Test] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RL-Test] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RL-Test] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RL-Test] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RL-Test] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RL-Test] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RL-Test] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RL-Test] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RL-Test] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RL-Test] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RL-Test] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RL-Test] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RL-Test] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RL-Test] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RL-Test] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RL-Test] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RL-Test] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RL-Test] SET RECOVERY FULL 
GO
ALTER DATABASE [RL-Test] SET  MULTI_USER 
GO
ALTER DATABASE [RL-Test] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RL-Test] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RL-Test] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RL-Test] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RL-Test] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RL-Test] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'RL-Test', N'ON'
GO
ALTER DATABASE [RL-Test] SET QUERY_STORE = OFF
GO
USE [RL-Test]
GO
/****** Object:  User [dbConnect]    Script Date: 1/08/2022 7:39:42 pm ******/
CREATE USER [dbConnect] FOR LOGIN [dbConnect] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [dbConnect]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [dbConnect]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [dbConnect]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [dbConnect]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [dbConnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [dbConnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [dbConnect]
GO
/****** Object:  Table [dbo].[Part]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Part](
	[Part_ID] [int] IDENTITY(1,1) NOT NULL,
	[Part_Name] [varchar](20) NOT NULL,
	[Part_Parent_ID] [int] NULL,
	[created_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Part_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Part_Properties]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Part_Properties](
	[Part_ID] [int] NOT NULL,
	[Property_Name] [varchar](50) NOT NULL,
	[Property_Value] [decimal](38, 3) NULL,
	[created_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Part_ID] ASC,
	[Property_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Part] ADD  DEFAULT (NULL) FOR [Part_Parent_ID]
GO
ALTER TABLE [dbo].[Part]  WITH CHECK ADD  CONSTRAINT [Part_FK] FOREIGN KEY([Part_Parent_ID])
REFERENCES [dbo].[Part] ([Part_ID])
GO
ALTER TABLE [dbo].[Part] CHECK CONSTRAINT [Part_FK]
GO
ALTER TABLE [dbo].[Part_Properties]  WITH CHECK ADD  CONSTRAINT [Part_Property] FOREIGN KEY([Part_ID])
REFERENCES [dbo].[Part] ([Part_ID])
GO
ALTER TABLE [dbo].[Part_Properties] CHECK CONSTRAINT [Part_Property]
GO
/****** Object:  StoredProcedure [dbo].[AddProperty]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[AddProperty] 
	-- Add the parameters for the stored procedure here
	@node_id int, 
	@parameter varchar(50),
	@value decimal(38,3),
	@code int = null output,
	@msg varchar(50) =null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRY
		INSERT INTO Part_Properties VALUES(@node_id, @parameter, @value, CURRENT_TIMESTAMP)
		SET @code = 200
		SET @msg = 'success'
		SELECT @code as code, @msg as msg
	END TRY
	BEGIN CATCH
		SET @code = ERROR_NUMBER()
		SET @msg = ERROR_MESSAGE()
		SELECT @code as code, @msg as msg
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[CreateChildNode]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreateChildNode] 
	-- Add the parameters for the stored procedure here
	@node_name varchar(20), 
	@parent_id int,
	@code int = null output,
	@code_message varchar(20) =null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRY
		INSERT INTO dbo.Part VALUES(@node_name, @parent_id, CURRENT_TIMESTAMP)
		SET @code = 200;
		SET @code_message = 'success'
		SELECT @code as code, @code_message as msg
		RETURN 
	END TRY
	BEGIN CATCH
		SET @code = ERROR_NUMBER()
		SET @code_message = ERROR_MESSAGE()
		SELECT @code as code , @code_message as msg
		RETURN
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[CreateParentNode]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreateParentNode]
	-- Add the parameters for the stored procedure here
	@node_name varchar(20), 
	@code int = null output,
	@code_message varchar(20) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN TRY
		INSERT INTO dbo.Part VALUES(@node_name, null, CURRENT_TIMESTAMP);
		SET @code = 200;
		SET @code_message = 'success';
		SELECT @code as code, @code_message as msg
		RETURN 
	END TRY
	BEGIN CATCH
		SET @code = ERROR_NUMBER()
		SET @code_message = ERROR_MESSAGE()
		SELECT @code as code, @code_message as msg
		RETURN
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[SelectAllParts]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ted
-- Create date: 30/07/2022
-- Description:	Select All Parts
-- =============================================
CREATE PROCEDURE [dbo].[SelectAllParts] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Part
END
GO
/****** Object:  StoredProcedure [dbo].[SelectChildrenFromParent]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[SelectChildrenFromParent] 
	-- Add the parameters for the stored procedure here
	@parent_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Part_ID,Part_Name,Part_Parent_ID,created_date from Part where Part_Parent_ID=@parent_id
END
GO
/****** Object:  StoredProcedure [dbo].[SelectSubTree]    Script Date: 1/08/2022 7:39:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SelectSubTree]
	-- Add the parameters for the stored procedure here
	@parent_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH nodedata AS (
	  (SELECT Part_ID, Part_Name, Part_Parent_ID, created_date, 1 AS level
	  FROM Part
	  WHERE Part_ID = @parent_id)
	  UNION ALL
	  (SELECT this.Part_ID, this.Part_Name, this.Part_Parent_ID, this.created_date, prior.level + 1
	  FROM nodedata prior
	  INNER JOIN Part this ON this.Part_Parent_ID = prior.Part_ID)
	)
	SELECT n.Part_ID, n.Part_Name, n.Part_Parent_ID, n.created_date as Part_Date, p.Property_Name, p.Property_Value, p.created_date as Property_Date,n.level
	FROM nodedata n LEFT OUTER JOIN Part_Properties p ON n.Part_ID=p.Part_ID
	ORDER BY n.level;
END
GO
USE [master]
GO
ALTER DATABASE [RL-Test] SET  READ_WRITE 
GO
