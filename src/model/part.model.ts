import { Model, DataTypes, ForeignKey } from "sequelize";
import { db } from '../utils/db-connection/connect';

class Part extends Model {
    declare Part_ID: number;
    declare Part_Name: string;
    declare Part_Parent_ID: ForeignKey<Part['Part_ID']>;
}

Part.init({
    Part_ID:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
    Part_Name:{
            type: DataTypes.STRING(128),
            allowNull: false,
        },
    Part_Parent_ID:{
            type: DataTypes.INTEGER,
            allowNull: true
        }
    },
    {
        tableName: 'Part',
        sequelize:db,
});


export default Part;