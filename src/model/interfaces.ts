import { DateDataType } from "sequelize/types";

export interface DBResponse {
    code: number;
    msg: string;
}

export interface DBResult extends DBResponse {
    body:TreeNode;
}

export interface Part {
    Part_ID: number;
    Part_Name: string;
    Part_Parent_ID: number;
    created_date: DateDataType;
}

export interface Property {
    Part_ID: number;
    Property_Name: string;
    Property_Value: number;
    created_date: DateDataType
}

export interface RawSubTree {
    Part_ID: number;
    Part_Name: string;
    Part_Parent_ID: number;
    Part_Date:DateDataType;
    Property_Name: string;
    Property_Value: number;
    Property_Date: DateDataType;
    level: number;
}

export interface TreeNode {
    id:number;
    name:string;
    date:DateDataType;
    properties: Property[];
    children: TreeNode[];
    parentID:number | null;
}

