import { number, object, string, z } from "zod";

export const createParentNodeSchema = object({
    body:object({
        name: string({
            required_error: "Name is required"
          }).max(20, "maximum of 20 characters")
    })
  });

export type createParentNodeInput =  z.infer<typeof createParentNodeSchema>

export const createChildNodeSchema = object({
    body:object({
        name: string({
            required_error: "Name is required"
          }).max(20, "maximum of 20 characters"),
        parent_id: number({
            required_error:"parent_id is required"
        }).nonnegative("id must be non-negative")
    })
  });

  export type createChildNodeInput =  z.infer<typeof createChildNodeSchema>

  export const createNodeParameterSchema = object({
    body:object({
      node_id: number({
        required_error:"node ID is required"
      }).nonnegative("id must be non-negative"),
      parameter: string({
          required_error: "Parameter Name is required"
        }).max(50, "maximum of 50 characters"),
      value: number({
          required_error:"Value is required"
      })
    })
  });

  export type createNodeParameterInput =  z.infer<typeof createNodeParameterSchema>

  export const getSubTreeSchema = object({
    body:object({
      path: string({
        required_error:"Path is required"
      }).regex(new RegExp('^[a-z A-Z0-9\/\-]+$'),"Only Some Characters allowed. only - / and alphanumeric values")
    })
  });

  export type getSubTreeInput =  z.infer<typeof getSubTreeSchema>