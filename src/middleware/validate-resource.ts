import {Request, Response, NextFunction } from 'express';
import { AnyZodObject }from 'zod';
import log from '../utils/logger/logger';

/**
 * checks every response has a valid format against a certain schema.
 * @param schema 
 * @returns 
 */
const validate = (schema: AnyZodObject) => (req: Request, res: Response, next:NextFunction) => {
    try {
        log.info(req.body);
        schema.parse({
            body: req.body
        })
        log.info("validated request")
        next();
    }
    catch (error) {
        return res.status(400).send(error)
    }
}

export default validate;