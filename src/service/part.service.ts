import { DBResponse, DBResult, Part, Property, RawSubTree, TreeNode } from "../model/interfaces";
import { db } from "../utils/db-connection/connect";
import log from "../utils/logger/logger";

export async function createParentNode(part_name:string){
    const response = await db.query(`CreateParentNode @node_name='${part_name}'`)
    .then( val => val[0][0] as DBResponse)
    .catch(e => e[0][0] as DBResponse);
    addParameterToNode(1,'test',12.234);
    return response;
}

export async function createChildNode(part_name:string, parent_id:number){
    part_name = part_name.trim().toLowerCase();
    const sameParents = await db.query(`SelectChildrenFromParent @parent_id=${parent_id}`).then(val => val[0] as Part[]);
    //Check has no name clash
    if (sameParents.filter(val => val.Part_Name == part_name).length === 0){
        const response = await db.query(`CreateChildNode @node_name='${part_name}', @parent_id=${parent_id}`)
        .then(val => val[0][0] as DBResponse
        )
        .catch(e => e[0][0] as DBResponse )
        return response;
    }
    else {
        return {code:409, msg:`Part Name ${part_name} is already in use.`} as DBResponse;
    }
}

export async function addParameterToNode(node_id:number,parameter:string,value:number){
    parameter = parameter.trim().toLowerCase();

    const nodeParameters = await db.query(`Select * From Part_Properties where Part_ID=${node_id}`).then(val => val[0] as Property[]);
    if (nodeParameters.filter(val => val.Property_Name === parameter).length === 0){
        const response = await db.query(`AddProperty @node_id=${node_id}, @parameter='${parameter}', @value=${value}`)
        .then(val => val[0][0] as DBResponse)
        .catch(e => {
            log.info(e);
            return {code:500, msg:`Sequelize Error. Check database query`}  as DBResponse
        }) 
        return response;
    }
    else {
        return {code:409, msg:`Parameter Name ${parameter} is already in use.`} as DBResponse;
    }
}

export async function getTree(path:string[]){
    log.info(path)
    let currentNode = null
    // get path https://docs.microsoft.com/en-us/sql/t-sql/statements/create-function-transact-sql?view=sql-server-ver16
    let queryString = path.length > 0 ? path[path.length-1] : null
    //search find tree up to last node
    for (let i=0;i<path.length - 1;i++ ){
        let query = currentNode === null 
        ? `Select * From Part Where Part_Parent_ID is ${currentNode}` 
        : `Select * From Part Where Part_Parent_ID=${currentNode}`;
        let currentPart:number = await db.query(query)
        
        .then(val => val[0] as Part[])
        .then((val) => {
            log.info(val);
            let nextID = val.filter( x => x.Part_Name.toLowerCase() === path[i].trim().toLowerCase())
            if (nextID.length === 1){
                return nextID[0].Part_ID
            }
            else {
                return -1;
            }
        })
        currentNode = currentPart
        if (currentPart < 0 ) {
            break 
        }
    }

    // eng <> engine -> engine
    // 
    //find matching last node
    if (currentNode === null || currentNode >= 0){
        let query = currentNode === null 
        ? `Select * From Part Where Part_Parent_ID is ${currentNode}` 
        : `Select * From Part Where Part_Parent_ID=${currentNode}`;
        let children = await db.query(query).then( val => val[0] as Part[])
        children = await children.filter( part => {
            if (queryString !== null ){
                return matching(queryString, part.Part_Name)
            }
            else {
                return false
            }
        })
        
        //if matching last node found, return that subtree
        if (children.length > 0){
            children.sort((x, y) => x.Part_Name.length - y.Part_Name.length)
            let tree = await db.query(`SelectSubTree @parent_id=${children[0].Part_ID}`)
            .then( val => val[0] as RawSubTree[])
            return {code:200, msg:'success', body:rawToTree(tree)} as DBResult;
        }
        

        //query string had no result throw error
        else {
            return {code:404, msg:`No matching Node for ${queryString}`, body: {}} as DBResult;
        }
    }

    //path not found throw error
    return {code:404, msg:`No matching path`, body: {}} as DBResult;
}

function matching(search:string,name:string){
    search = search.trim().toLowerCase()
    name = name.trim().toLowerCase()
    return name.startsWith(search)
}

function rawToTree(raw:RawSubTree[]){
    let root:TreeNode = {} as TreeNode;
    var dictionary: { [id: string] : TreeNode; } = {};
    for(let [index, part] of raw.entries()){
        if (dictionary[part.Part_ID] === undefined){
            let node = mapRawToNode(part)
            dictionary[part.Part_ID] = node;
            if (index !== 0 && node.parentID !== null){
                dictionary[node.parentID.toString()].children.push(node)
            }
            else{
                root = node
            }
        } else {
            let property = {
                Part_ID: part.Part_ID,
                Property_Name: part.Property_Name,
                Property_Value: part.Property_Value,
                created_date: part.Property_Date
            } as Property;
            dictionary[part.Part_ID].properties.push(property);
        }
    }
    return root;
}

function mapRawToNode(raw:RawSubTree){
    let node = {
        id:raw.Part_ID,
        name:raw.Part_Name,
        date:raw.Part_Date,
        created_date:raw.Part_Date,
        properties:[{
            Part_ID:raw.Part_ID,
            Property_Name:raw.Property_Name,
            Property_Value:raw.Property_Value,
            created_date:raw.Property_Date
        }],
        children:[],
        parentID:raw.Part_Parent_ID
    } as TreeNode;
    return node;
}