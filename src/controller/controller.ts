import { Request, Response } from "express";
import { createChildNodeInput, createNodeParameterInput, createParentNodeInput, getSubTreeInput } from "../schema/schema";
import { addParameterToNode, createChildNode, createParentNode, getTree } from "../service/part.service";
import log from "../utils/logger/logger";

export async function createParentNodeHandler(
    req: Request<{}, {}, createParentNodeInput["body"]>,
    res: Response
  ) {
    try {
        const dbResponse = await createParentNode(req.body.name);
        if( dbResponse.code === 200){
            log.info('successful creation')
            return res.sendStatus(200);
        }
        else{
            return res.status(500).send(dbResponse.msg)
        }
    } catch (e: any) {
      log.error(e);
      return res.status(409).send(e.message);
    }
  }

export async function createChildNodeHandler(
    req: Request<{}, {}, createChildNodeInput["body"]>,
    resp: Response
) {
    try {
        const dbResponse = await createChildNode(req.body.name, req.body.parent_id);
        if( dbResponse.code === 200){
            log.info('successful creation')
            return resp.sendStatus(200);
        }
        else{
            return resp.status(500).send({error:dbResponse.msg})
        }
    } catch (e: any) {
      log.error(e);
      return resp.status(409).send({error:e.message});
    }
}

export async function addParameterHandler(
    req: Request<{}, {}, createNodeParameterInput["body"]>,
    resp: Response
){
    try {
        const dbResponse = await addParameterToNode(req.body.node_id, req.body.parameter, req.body.value);
        if( dbResponse.code === 200){
            log.info('successful creation')
            return resp.sendStatus(200);
        }
        else{
            return resp.status(500).send({error:dbResponse.msg})
        }
    } catch (e: any) {
      log.error(e);
      return resp.status(409).send({error:e.message});
    }
}

export async function getSubTreeHandler(
    req: Request<{}, {},getSubTreeInput["body"]>,
    resp: Response
){
    try {
        const tree = await getTree(req.body.path.split("/"))
        if (tree.code === 200){
            return resp.json(tree.body).status(200).send();
        }
        else {
            return resp.status(tree.code).send({error:tree.msg});
        }
    }
    catch (e:any ) {
        log.error(e);
        return resp.status(500).send({error:e.message});
    }
};
