import express from 'express';
import config from 'config';
import log from './utils/logger/logger';
import { connect } from './utils/db-connection/connect';
import routes from './routes';
import cors, { CorsOptions } from 'cors';

const port = config.get<number>('port');
const host = config.get<string>('host');
const corsOoptions = config.get<CorsOptions>('cors');
const app = express();

app.use(cors(corsOoptions))
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.listen(port, host, async () => {
    //use dependency for logger to prevent logging blocking the i/o as nodejs is singlely threaded
    log.info(`server listening at http://${host}:${port}`);

    await connect();

    routes(app);
})
