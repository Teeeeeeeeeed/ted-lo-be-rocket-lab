import { Express, Request, Response } from 'express';
import { db } from './utils/db-connection/connect';

import { createChildNodeSchema, createNodeParameterSchema, createParentNodeSchema, getSubTreeSchema } from './schema/schema';
import validate from './middleware/validate-resource';
import { createChildNodeHandler, addParameterHandler, createParentNodeHandler, getSubTreeHandler } from './controller/controller';
import log from './utils/logger/logger';

function routes(app: Express) {
    app.get('/healthcheck', (req: Request, resp: Response) => {
        resp.sendStatus(200);
    });

    app.get('/parts', (req: Request, resp: Response) => {
        db.query('SelectAllParts').then( x => log.info(x)).catch(error=>log.info(error))
    })

    app.post("/create-parent-parts", validate(createParentNodeSchema), createParentNodeHandler)
    
    app.post("/create-child-parts", validate(createChildNodeSchema), createChildNodeHandler)

    app.post("/add-node-parameter", validate(createNodeParameterSchema), addParameterHandler)

    app.post("/get-sub-tree", validate(getSubTreeSchema), getSubTreeHandler)
}

export default routes;