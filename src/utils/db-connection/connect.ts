import { Sequelize } from 'sequelize';
import config from 'config';
import log from '../logger/logger';

const database = config.get<string>('database');
const userName = config.get<string>('userName');
const password = config.get<string>('password');

const options = config.get<Object>('options');

const db = new Sequelize(database,userName,password, options);

async function connect() {
  await db.authenticate(options).then(()=>{
    log.info('Connection has been established successfully.');
    return db;
  }).catch(error => {
    log.error(`Unable to connect to the database:${database}`, error);
    return null
  });
}

export {connect, db};