### Backend Development

# Important Steps to run the application

- run the 'script.sql' file to populate database and database objects such as the stored procedures and database login.
- run `npm i` to install used dependencies.

## Due to time constraints on my behalf unit tests were not written

## References Used

### [Storing Hierarchy Data in Relational Databases](https://www.databasestar.com/hierarchical-data-sql/)

### [Stack overflow discussion of hierarchy data in RBDMS](https://stackoverflow.com/questions/4048151/what-are-the-options-for-storing-hierarchical-data-in-a-relational-database)

### [Node JS Basics (W3School)](https://www.w3schools.com/nodejs/default.asp)

### [Node JS Basics](https://nodejs.dev/learn)

### [Sequelize](https://sequelize.org/api/v6/identifiers)

### [Schema validation](https://morioh.com/p/cc9d89e8a10b)